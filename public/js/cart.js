let carts = document.querySelectorAll('.add-cart');
for(let i=0; i<carts.length; i++){
    carts[i].addEventListener("click",() =>{
        cartNumbers(products[i]);  
        totalCost(products[i]);
    })
}
let products = [
    {
        name:"HyperX Alloy FPS",
        tag: "kb1",
        price:3500,
        inCart:0
    },
    {
        name:"Razer Huntsman",
        tag: "kb1",
        price:4500,
        inCart:0
    },
    {
        name:"Razer Ornata",
        tag: "kb1",
        price:5500,
        inCart:0
    },
    {
        name:"Razer Viper",
        tag: "mouse1",
        price:2869,
        inCart:0
    },
    {
        name:"Logitech G102",
        tag: "mouse1",
        price:999,
        inCart:0
    },
    {
        name:"Steel Series Rival",
        tag: "mouse1",
        price:2095, 
        inCart:0
    },
    {
        name:"HyperX Stringer",
        tag: "headset1",
        price:2500,
        inCart:0
    },
    {
        name:"HyperX Cloud II",
        tag: "headset1",
        price:2090,
        inCart:0
    },
    {
        name:"HyperX Cloud",
        tag: "headset1",
        price:5040,
        inCart:0
    }
];

function onLoadCartNumbers(){
    let productNumbers = localStorage.getItem('cartNumbers');
    if (productNumbers){
       document.querySelector('.cart span').textContent = productNumbers; 
    }
}

function cartNumbers(product) {
    let productNumbers = localStorage.getItem('cartNumbers');
    productNumbers = parseInt(productNumbers);

    if(productNumbers){
    localStorage.setItem('cartNumbers', productNumbers + 1);
    document.querySelector('.cart span').textContent = productNumbers + 1;

    } else {
    localStorage.setItem('cartNumbers', 1);
    document.querySelector('.cart span').textContent = 1;
    }
    setItems(product);
}


function setItems(product){
    let cartItems = localStorage.getItem('productsInCart');
    cartItems = JSON.parse(cartItems);
    
    if(cartItems != null){
        if(cartItems[product.tag] == undefined){
            cartItems = {
                ...cartItems,
                [product.tag]: product
            }
        }
        cartItems[product.tag].inCart += 1;
    } else {
        product.inCart = 1;
        cartItems = {
        [product.tag]: product
        }
    }
    
    localStorage.setItem("productsInCart", JSON.stringify(cartItems));
}

/*
function totalCost(product){
//console.log(product.price);
let cartCost = localStorage.getItem("totalCost");
cartCost = parseInt(cartCost);
console.log("Cart", cartCost);

if(cartCost != null){
cartCost = parseInt(cartCost);
localStorage.setItem("totalCost", cartCost + 
product.price);
}else{
    localStorage.setItem("totalCost",product.price);
}

}
*/function totalCost (product){
    console.log("the product price is" , product.price);
    let cartCost = localStorage.getItem('totalCost');

    if(cartCost != null){
        cartCost = parseInt(cartCost);
        localStorage.setItem("totalCost", cartCost + product.price);
    } else {
        localStorage.setItem("totalCost", product.price);
    }
}

function displayCart(){
    let cartItems = localStorage.getItem("productsInCart");
    cartItems = JSON.parse(cartItems);
    let productContainer = document.querySelector(".products");
    let cartCost = localStorage.getItem('totalCost');

    if(cartItems && productContainer){
        productContainer.innerHTML = '';
        Object.values(cartItems).map(item => {
            productContainer.innerHTML += `
            <div class="product">
                <ion-icon name="close-circle"></ion-icon>
                <img src="images/${item.tag}.png">
                <span>${item.name}</span>
            </div>
            <div class="price">${item.price}</div>
            <div class="quantity">
                <h1 class="decrease" 
                 name="arrow-dropleft-circle">+</h1>
                <span>${item.inCart}</span>
                <h1 class="increase"
                 name="arrow-dropright-circle">-</h1>
            </div>
            <div class="total">
                ${item.inCart * item.price}.00
            </div>
            `;
        });
        productContainer.innerHTML += `
            <div class="basketTotalContainer">
                <h4 class="baskektTotalTitle">
                Total 
                </h4>
                <h4 class="basketTotal">
                ₱${cartCost}.00 
                </h4>
        `;
    }
}
onLoadCartNumbers();
displayCart();